#ifndef ALLOCATOR
#define ALLOCATOR
#include "rider/faiz/cstddef.hpp"
#include "rider/faiz/macros.hpp"
#include "rider/faiz/type_traits.hpp"
#include "rider/faiz/utility.hpp"
namespace Rider::Faiz
{} // namespace Rider::Faiz
#endif
